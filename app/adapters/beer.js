import DS from 'ember-data';

export default DS.RESTAdapter.extend({
  host: 'https://api.punkapi.com',
  namespace: 'v2',

  pathForType() {
    return 'beers';
  }
});
