import DS from 'ember-data';

const { attr } = DS;
export default DS.Model.extend({
  name: attr('string'),
  tagline: attr('string'),
  description: attr('string'),
  image_url: attr('string'),
  brewers_tips: attr('string'),
  ibu: attr('string'),
  abv: attr('string')
});
